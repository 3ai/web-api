module WebApi.Server.Functions
       ( -- * helper functions
         handleJson
       ) where

import WebApi.Server.Types

-- | Helper function for safe writing request handlers. Function
-- guarantees that request and response data will be at least from one
-- 'Endpoint' instance in your handler.
handleJson :: (JsonHandler m a)
           => (JHRequest a -> m (JHResponse a))
           -> m ()
handleJson action = do
    req <- parseRequest
    resp <- action req
    encodeResponse resp
