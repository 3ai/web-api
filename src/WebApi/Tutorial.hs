module WebApi.Tutorial
       (
         -- * Common code
         -- $common

         -- * Server side
         -- $server
       ) where


{- $common

For example, you have some service for managing user notes. It can create,
read, update and delete user notes. You have to create two packages :
__notes-server__ and __notes-client__. Client will talk with server by JSON
api, so, on server side you have routes like that:

@
\/api\/v1\/notes                NotesR       GET
\/api\/v1\/notes\/public         NotesPublicR GET
\/api\/v1\/notes\/create         NotesCreateR POST
\/api\/v1\/note\/#NoteId         NoteR        GET
\/api\/v1\/note\/#NoteId\/update  NoteUpdateR  POST
\/api\/v1\/note\/#NoteId\/delete  NoteDeleteR  POST
@

This example is for Yesod, but __web-api__ does not depend on Yesod or any
other web server. But __web-api__ depends on __http-conduit__ which is the
default client implementation.

Lets consider just one endpoint, note updating for example. To update note
you need several things:

* Generate correct path on client side
* Generate request body with data to update note on client side
* Parse request body on server side to understand what client wants
* Generate response on server side
* Parse server response on client type to understand what happened on server
  and get data.
* You want to do it type safe so the type used to genrate request on client
  side must be at least isomorphic to type on server side. And vise versa.

We want to have one type for request data on client and server side, so
let's move this type to separate package and import this package on both,
server and client. Call this package __notes-api__.

Here is data which we will use for query:

@
data NoteUpdate =
    NoteUpdate
    { nuPublicity :: !(Maybe Bool)
    , nuText      :: !(Maybe Text)
    } deriving (Eq, Ord, Show, Read, Typeable)
@

What this type tells us? To update note we must give the server optional
publicity flag and optional note's text. After successfull note updating
server must give us updated note:

@
data UserNote =
    UserNote
    { noteId        :: Text
    , noteUserId    :: Text
    , notePublicity :: Bool
    , noteText      :: Text
    } deriving (Eq, Ord, Show, Read, Typeable)
@

We also want to generate the path __with note id__ in it, so we create type
like that:

@
data Api1NoteUpdate =
    Api1NoteUpdate
    { nuNoteId :: !Text
    } deriving (Ord, Eq, Read, Show, Typeable)
@

__Api1NoteUpdate__ is used to generate path. We could create function like
that:

@
api1NoteUpdatePath :: Api1NoteUpdate -> [Text]
api1NoteUpdatePath path = do
    ["api", "v1", "note", nuNoteId path, "update"]

> λ> api1NoteUpdatePath $ Api1NoteUpdate "1234"
> ["api","v1","note","1234","update"]
@

But it is not very convenient, so, lets define typeclass for generating
routes from our types

@
-- | Instances of this typeclass represents path to some service
class Route a where
    -- | Method to generate path to the endpoint on client side. This code
    -- is usually used for client.
    routePath :: a -> [Text]
@

And write instance of __Route Api1NoteUpdate__

@
instance Route Api1NoteUpdate where
    routePath path =
        [ "api", "v1", "note" , nuNoteId path, "update"]
@

Ok. Now we can generate path for our service like:

@
λ> mappend "\/" $ T.intercalate "\/" $ routePath $ Api1NoteUpdate "jdjd"
"\/api\/v1\/note\/jdjd\/update"
@

Next goal is to generate request body for our service and parse it's
response. We could do it manually, but this is not very typesafe. Your
server can generate wrong response as well as client can generate wrong
request. To stick types to endpoint lets introduce a typeclass:

@
class (Route a) => Endpoint a where
    -- | Data of this type must generated on client side and parsed on
    -- server side. Type can be encoded as JSON, list of parameters, or
    -- XML, this does not matters for this typeclass.
    data Req a      :: *

    -- | Data of this type generated on server side as successfull response
    -- and parsed on client side. Typically, response considered
    -- successfull when http status is 200.
    data Resp a     :: *

    -- | Data of this type is generated on server side as error response
    -- and parsed on client side. Typically, response considered to be
    -- error response when http status is not 200.
    type ErrResp a  :: *

    -- | Returns http method of request.
    endpointMethod  :: a -> Method

-- | Parsed response from server. Which is either server error description
-- or successfull response
type EPResponse a = Either (ErrResp a) (Resp a)
@

And make an instance for our route type:

@
data NoteUpdate

data NoteUpdateResponse

data ErrorResponse

instance Endpoint Api1NoteUpdate where
    newtype Req Api1NoteUpdate = NUReq NoteUpdate

    newtype Resp Api1NoteUpdate = NUResp NoteUpdateResponse

    type ErrResp Api1NoteUpdate = ErrorResponse

    endpointMethod _ = "POST"
@

Now we have declared type of request and both responses: error response and
correct data response. We do not cary about how this types parsed and
generated, this is just to stick types to endpoint and work with same types
on both client and server side.

Lets declare our types more specific:

@
data NoteUpdate =
    NoteUpdate
    { nuPublicity :: !(Maybe Bool)
    , nuText      :: !(Maybe Text)
    } deriving (Eq, Ord, Show, Read, Typeable)

data UserNote =
    UserNote
    { noteId        :: Text
    , noteUserId    :: Text
    , notePublicity :: Bool
    , noteText      :: Text
    } deriving (Eq, Ord, Show, Read, Typeable)

data EntityResponse a =
    EntityResponse
    { erResult  :: !Bool
    , erReply   :: !a
    , erContext :: !Text
    } deriving (Ord, Eq, Show, Read, Typeable)

data ErrorResponse
    = ERUnknown
    | ERServerError { erMessage :: Text }
    | ERTeapot { erVolume :: Double }

instance Endpoint Api1NoteUpdate where
    newtype Req Api1NoteUpdate
        = NUReq NoteUpdate
        deriving (Ord, Eq)
    newtype Resp Api1NoteUpdate
        = NUResp (EntityResponse UserNote)
        deriving (Ord, Eq)
    type ErrResp Api1NoteUpdate = ErrorResponse
    endpointMethod _ = "POST"
@

Note how we used __EntityResponse__. Now we can reuse it in several
responses just changing it's type parameter.

Ok. Lets generate our first request to the server. This is again new
typeclass:

@
class Endpoint a => JsonEndpoint a where
    -- | Response parser error. If response from server is not parsed
    -- then you will get error of this type.
    type JEErr a :: *

    -- | Request modificator. Get initial request and substitute necessary
    -- path, query, headers and maybe other fields
    encodeJsonRequest :: Req a   -- ^ endpoint request type
                      -> Request -- ^ Initial request
                      -> Request

    -- | Response parser. Assuming response is not huge bulk of data, but
    -- just Json encoded data
    parseJsonResponse :: Response BL.ByteString -- ^ Server response
                      -> JResponse a

-- | Either parser error or response from the server, which in turn
-- can be successfull or error response also.
type JResponse a = Either (JEErr a) (EPResponse a)
@

__Request__ is from __http-conduit__ package. We use it by default, but you
can define your own typeclass and instances for it. Here 'JEErr' is a type
of response parsing error. If you are using aeson to parse JSON response
this type will be 'String', other parsers can return more structured error
description.

Now lets define intance for our __Api1NoteUpdate__:

@
instance JsonEndpoint Api1NoteUpdate where
    type JEErr Api1NoteUpdate = String
    encodeJsonRequest rdata req =
        req { requestHeaders =
                      ("content-type", "application/json")
                      : requestHeaders req
            , requestBody = RequestBodyLBS
                            $ encode rdata
            }

    parseJsonResponse resp =
        case statusCode $ responseStatus resp of
            200 -> tryParse resp Right
            _   -> tryParse resp Left
      where
        tryParse r side = side
                          <$> eitherDecode (responseBody r)
@

In real world we would use default implementations of method
'encodeJsonRequest' and 'parseJsonResponse' provided in module
"WebApi.Client.Types", but it would be less visually.

Now we can write a function using our information from types:

@
queryJson :: (JsonEndpoint a)
          => Manager             -- ^ Http manager
          -> Request             -- ^ Initial request with host:port and
          -- other parameters. This method will replace just path, query,
          -- method and checkStatus fields
          -> a              -- ^ endpoint itself
          -> Req a          -- ^ request data
          -> IO (JResponse a)
    let req' = encodeJsonRequest rdata r
        req = req' { checkStatus = \_ _ _ -> Nothing
                   , method = endpointMethod endp
                   , path = toByteString
                            $ encodePathSegments
                            $ routePath endp
                   , requestHeaders =
                       ("accept", "application/json")
                       : requestHeaders req'
                   }
    resp <- httpLbs req man
    return $ parseJsonResponse resp
@

Very simple. Now this function can be used like:

@
let route = Api1NoteUpdate noteId
    rdata = NUReq
            $ NoteUpdate Nothing (Just "text")
NUResp note <- queryJson manager req route rdata
               >>= handleErrors
@

where 'handleErrors' is just some function of such type:

@
handleErrors :: Either (JEErr a) (Either (ErrResp a) (Resp a))
             -> IO (Resp a)
@

which handles parser or server error or return successfull request.

Note, that types of __rdata__ and __note__ are sticked to __NoteUpdate__
and __(EntityResponse UserNote)__ respectively just because the type of
__route__ is __Api1NoteUpdate__. So, our client is completely safe!

All of this code except typeclasses which are implemented in __web-api__
must be placed in some common package __notes-api__ which is dependency of __notes-server__ and __notes-client__ simultaneously.

-}

{- $server

To handle this request on server side we need to parse request and generate
response. Server side handlers is usually a reader monad, like in Yesod and
Scotty, which gives an access to the request.

@
class (Monad m) => JsonHandler m a where
    type JHErr a :: *
    parseRequest :: m (JHRequest a)
    encodeResponse :: JHResponse a -> m ()

type JHRequest a = Either (JHErr a) (Req a)
type JHResponse a = Either (ErrResp a) (Resp a)
@

This is typeclass of two parameters: handler monad which has methods to
access request and route type. Here is an instance:

@
instance JsonHandler Handler Api1NoteUpdate where
    type JHErr Api1NoteUpdate = String
    parseRequest = do
        res <- parseJsonBody
        return $ case res of
            (Error e) -> Left e
            (Success a) -> Right a
    encodeResponse = either
                     atsResponse
                     (sendResponse . toJSON)
      where
        atsResponse a = sendResponseStatus
                        (atsStatus a)
                        $ toJSON a
          where
            atsStatus = error "Implement: status generation function"
@

Using this simple usefull function:

@
handleJson :: (JsonHandler m a)
           => (JHRequest a -> m (JHResponse a))
           -> m ()
handleJson action = do
    req <- parseRequest
    resp <- action req
    encodeResponse resp
@

we can constraint types and implement safe server side handler:

@
postNoteUpdateR :: NoteId -> Handler ()
postNoteUpdateR nid = handleJson $ \ereq -> runExceptT $ do
    NUReq noteUpdate <- either
                        parserError
                        return
                        ereq
    .........
    -- performing note updating
    .........

    return $ NUResp
           $ EntityResponse
           { erResult = True
           , erReply  = UserNote { noteId = nid
                                 , noteUserId = uid
                                 , notePublicity = publicity
                                 , noteText = noteText }
           , erContext = "note" }
@

Note, that because of pattern matching with __NUReq__ in the begining __noteUpdate__ has a type __NoteUpdate__ and response is __(EntityResponse UserNote)__. Handler will not compiled if we would used other types.

-}
