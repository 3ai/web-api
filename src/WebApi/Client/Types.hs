module WebApi.Client.Types
       ( -- * Typeclasses
         JsonEndpoint(..)
       , JResponse
         -- * Default method implementations
       , encodeJsonRequestPost
       , encodeRequestGet
       , encodeRequestPost
       , encodeJsonRequestNone
       , parseJsonResponseDefault
       ) where

import Control.Applicative
import Data.Aeson
import Data.Maybe ( catMaybes )
import Network.HTTP.Conduit
import Network.HTTP.Types
import WebApi.Common.Types

import qualified Data.ByteString.Lazy as BL

-- | This code is used on client side to query the server and
-- parse response from.
--
-- Here is the example of implementation:
--
-- @
-- instance JsonEndpoint Api1NoteUpdate where
--     type JEErr Api1NoteUpdate = String
--     encodeJsonRequest = encodeJsonRequestPost
--     parseJsonResponse = parseJsonResponseDefault
-- @
--
-- This is all you need to perform requests to the server and parse
-- responses from. Also, you will need an instance of 'Endpoint' for
-- this type.
--
-- This typeclass is assigned to http-conduit package to reduce code
-- complexity, but in future this may be changed.
class Endpoint a => JsonEndpoint a where
    -- | Response parser error. If response from server is not parsed
    -- then you will get error of this type.
    type JEErr a :: *

    -- | Request modificator. Get initial request and substitute necessary
    -- path, query, headers and maybe other fields
    encodeJsonRequest :: Req a   -- ^ endpoint request type
                      -> Request -- ^ Initial request
                      -> Request

    -- | Response parser. Assuming response is not huge bulk of data, but
    -- just Json encoded data
    parseJsonResponse :: Response BL.ByteString -- ^ Server response
                      -> JResponse a

-- | Either parser error or response from the server, which in turn
-- can be successfull or error response also.
type JResponse a = Either (JEErr a) (EPResponse a)

-- | Default implementation of method 'encodeJsonRequest'. Encode
-- request data as JSON in request body. Also set header
-- \"content-type\"
encodeJsonRequestPost :: (JsonEndpoint a, ToJSON (Req a))
                      => Req a
                      -> Request
                      -> Request
encodeJsonRequestPost rdata req =
    req { requestHeaders =
                  ("content-type", "application/json")
                  : requestHeaders req
        , requestBody = RequestBodyLBS
                        $ encode rdata
        }

-- | Default implementation of method 'encodeJsonRequest'. Encode
-- request data as list of parameters in request query part. Modify
-- just 'queryString' of Request
encodeRequestGet :: (Endpoint a, QueryLike (Req a))
                 => Req a
                 -> Request
                 -> Request
encodeRequestGet rdata req =
    setQueryString (toQuery rdata) req

-- | Default implementation of method 'encodeJsonRequest'. Encode 'Req
-- a' as request body parameters aka Post parameters.
encodeRequestPost :: (Endpoint a, QueryLike (Req a))
                  => Req a
                  -> Request
                  -> Request
encodeRequestPost rdata req =
    let q = toQuery rdata
        mfun (a, b) = maybe Nothing (Just . (a,)) b
        rqbody = catMaybes $ map mfun q
    in urlEncodedBody rqbody req

-- | Default implementation of method 'encodeJsonRequest'. Does just
-- nothing with request. Your 'Endpoint' instance should define 'Req'
-- as () in this case. Anyway it will not be encoded in request.
--
-- @
-- instance Endpoint Api1NoteDetails where
--     newtype Req Api1NoteDetails =
--         NDReq ()
-- ................
-- ................
--
-- instance JsonEndpoint Api1NoteDetails where
--     type JEErr Api1NoteDetails = String
--     encodeJsonRequest = encodeJsonRequestNone
--     parseJsonResponse = parseJsonResponseDefault
-- @
encodeJsonRequestNone :: Req a -> Request -> Request
encodeJsonRequestNone _ req = req

-- | Default implementation of method 'parseJsonResponse'. If response
-- status is 200 then try parse response as successfull 'Resp' else
-- try parse as 'ErrResp'. Return 'String' as parsing error type
parseJsonResponseDefault :: (JsonEndpoint a, String ~ JEErr a
                           , FromJSON (Resp a), FromJSON (ErrResp a))
                         => Response BL.ByteString
                         -> JResponse a
parseJsonResponseDefault resp =
    case statusCode $ responseStatus resp of
        200 -> tryParse resp Right
        _   -> tryParse resp Left
  where
    tryParse r side = side
                      <$> eitherDecode (responseBody r)
