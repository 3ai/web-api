module WebApi.Server.Types
       ( -- * typeclasses
         JsonHandler(..)
       , JHRequest
       , JHResponse
       ) where

import WebApi.Common.Types

-- | Helper typeclass for implementing API on server side. Because of
-- data families 'Req' and 'Resp' you will never use wrong
-- request/response type. Monad 'm' here is a handler monad of you web
-- framework, usually it is a reader with access to request data, such
-- as headers and request body.
--
-- Here is an example of implementing with Yesod:
--
-- @
-- instance JsonHandler Handler Api1NoteUpdate where
--     type JHErr Api1NoteUpdate = String
--     parseRequest = do
--         res <- parseJsonBody
--         return $ case res of
--             (Error e) -> Left e
--             (Success a) -> Right a
--     encodeResponse = either
--                      atsResponse
--                      (sendResponse . toJSON)
--       where
--         atsResponse a = sendResponseStatus
--                         (atsStatus a)
--                         $ toJSON a
--           where
--             atsStatus = error "Implement: status generation function"
-- @
class (Monad m) => JsonHandler m a where
    -- | Request parsing error type. If you are using aeson to parse
    -- request then parser error is usually is 'String'
    type JHErr a :: *

    -- | Parse request to Haskell type
    parseRequest :: m (JHRequest a)

    -- | Send response to the client
    encodeResponse :: JHResponse a -> m ()

-- | Either parsed request or parser error
type JHRequest a = Either (JHErr a) (Req a)

-- | Either successfull response or error response
type JHResponse a = Either (ErrResp a) (Resp a)
